class SessionsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:new, :create]

  def new
    @user = User.new
  end

  def create
    @user = User.where(email: params[:user][:email])
    if @user.any? && @user.first.authenticate(params[:user][:password])
      session[:user_id] = @user.first.id
      flash[:notice] = "Ingreso exitoso. Hola #{@user.first.name}!"
    else
      flash[:alert] = "Error: Los datos ingresados no corresponden a ningún usuario."
      redirect_to new_session_path and return
    end
    redirect_to root_path
  end

  def destroy
    session.delete(:user_id)
    flash[:notice] = "Sesión finalizada"
    redirect_to root_path
  end
end
