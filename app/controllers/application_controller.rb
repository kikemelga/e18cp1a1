class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  private

  def authenticate_user!
    unless user_signed_in?
      flash[:alert] = "Debe ingresar como usuario para poder acceder a esta sección."
      redirect_to new_session_path
    end
  end

  def user_signed_in?
    session[:user_id].present?
  end

  def current_user
    if session[:user_id]
      User.find(session[:user_id])
    end
  end
  helper_method :user_signed_in?
  helper_method :current_user
end
