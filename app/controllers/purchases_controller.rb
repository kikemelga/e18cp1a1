class PurchasesController < ApplicationController
  def create
    @product = Product.find(params[:product_id])
    Purchase.create(user_id: current_user.id, product_id: @product.id)
    flash[:notice] = "Has comprado #{@product.name}"
    redirect_to root_path
  end
end
