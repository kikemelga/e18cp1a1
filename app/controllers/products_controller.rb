class ProductsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index]

  def index
    @products = Product.all
  end

  def new
    @product = Product.new
  end

  def show
    @product = Product.find(params[:id])
  end

  def create
    @product = Product.new(product_params)
    @product.user_id = current_user.id
    if @product.save
      flash[:notice] = "Hemos publicado tu aviso de #{@product.name}!"
      redirect_to root_path
    else
      flash[:alert] = "Error: " + @product.errors.full_messages.join(', ')
      redirect_to new_product_path
    end

  end

  private

  def product_params
    params.require(:product).permit(:name, :image, :price, :description)
  end
end
