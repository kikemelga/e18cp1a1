module ApplicationHelper
  def active_if(controller, action)
    'active' if current_page?(controller: controller, action: action)
  end
end
