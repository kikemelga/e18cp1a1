module ProductsHelper
  def is_purchased?(product)
    Purchase.where(product_id: product.id).any?
  end

  def product_owner(product)
    User.find(product.user_id)
  end
end
